class SStringTest {
  val internalVariable = 5

  def nullStringHandler(str: String): Unit = {
    if (str==null || str.isBlank) { println("No string provided")}
    else { println(str) }
  }
}
